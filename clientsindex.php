<?php
/* Copyright (C) 2001-2005 Rodolphe Quiedeville <rodolphe@quiedeville.org>
 * Copyright (C) 2004-2015 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) 2005-2012 Regis Houssin        <regis.houssin@inodbox.com>
 * Copyright (C) 2015      Jean-François Ferry	<jfefe@aternatik.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *	\file       clients/clientsindex.php
 *	\ingroup    clients
 *	\brief      Home page of clients top menu
 */

// Load Dolibarr environment
$res = 0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (!$res && !empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) {
	$res = @include $_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php";
}
// Try main.inc.php into web root detected using web root calculated from SCRIPT_FILENAME
$tmp = empty($_SERVER['SCRIPT_FILENAME']) ? '' : $_SERVER['SCRIPT_FILENAME']; $tmp2 = realpath(__FILE__); $i = strlen($tmp) - 1; $j = strlen($tmp2) - 1;
while ($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i] == $tmp2[$j]) {
	$i--; $j--;
}
if (!$res && $i > 0 && file_exists(substr($tmp, 0, ($i + 1))."/main.inc.php")) {
	$res = @include substr($tmp, 0, ($i + 1))."/main.inc.php";
}
if (!$res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i + 1)))."/main.inc.php")) {
	$res = @include dirname(substr($tmp, 0, ($i + 1)))."/main.inc.php";
}
// Try main.inc.php using relative path
if (!$res && file_exists("../main.inc.php")) {
	$res = @include "../main.inc.php";
}
if (!$res && file_exists("../../main.inc.php")) {
	$res = @include "../../main.inc.php";
}
if (!$res && file_exists("../../../main.inc.php")) {
	$res = @include "../../../main.inc.php";
}
if (!$res) {
	die("Include of main fails");
}

require_once DOL_DOCUMENT_ROOT.'/core/class/html.formfile.class.php';

// Load translation files required by the page
$langs->loadLangs(array("clients@clients"));

$action = GETPOST('action', 'aZ09');


// Security check
$socid = GETPOST('socid', 'int');
if (isset($user->socid) && $user->socid > 0) {
	$action = '';
	$socid = $user->socid;
}

$max = 5;
$now = dol_now();


/*
 * Actions
 */

// None


/*
 * View
 */

$form = new Form($db);
$formfile = new FormFile($db);

llxHeader("", $langs->trans("ClientsArea"));

print load_fiche_titre($langs->trans("ClientsArea"), '', 'clients.png@clients');

print '<div class="fichecenter"><div class="fichethirdleft">';


require_once DOL_DOCUMENT_ROOT."/commande/class/commande.class.php";
require_once DOL_DOCUMENT_ROOT."/societe/class/societe.class.php";
require_once DOL_DOCUMENT_ROOT."/product/class/product.class.php";
require_once DOL_DOCUMENT_ROOT."/categories/class/categorie.class.php";
$lesproduits = new Product($db);
$lessocietes = new Societe($db);
$lescommandes = new Commande($db);
$lescategories = new Categorie($db);


// meilleurs clients

if (! empty($conf->clients->enabled) && $user->rights->clients->client->read)

{
	$langs->load("orders");


// ajout du premier client dans la categorie
$sql = "SELECT c.rowid, c.ref, c.fk_soc, c.total_ttc, s.nom, s.rowid, SUM(c.total_ht) as mtotal, cs.fk_categorie, cs.fk_soc as csoc";
$sql .= " FROM ".MAIN_DB_PREFIX."commande as c";
$sql .= ", ".MAIN_DB_PREFIX."societe as s";
$sql .= ", ".MAIN_DB_PREFIX."categorie_societe as cs";
$sql .= " WHERE c.fk_soc = s.rowid";
$sql .= " GROUP BY s.nom";
$sql .= " ORDER BY mtotal DESC";
$sql .= " LIMIT 1";

$resql = $db->query($sql);

if ($resql)
{
	$obj = $db->fetch_object($resql);

	$sql = "REPLACE INTO ".MAIN_DB_PREFIX."categorie_societe (fk_categorie, fk_soc)";
	$sql .= " VALUES (";
	$sql .= "207,";
	$sql .= "'".$obj->csoc."'"; 
	$sql .= ")";
	if (!$db->query($sql)) {
		dol_print_error($db);
		$error++;
	}

}


	// affichage des informations
	$sql = "SELECT c.rowid, c.ref, c.fk_soc, c.total_ttc, s.nom, s.rowid, SUM(c.total_ht) as mtotal, cs.fk_categorie, cs.fk_soc as csoc";
	$sql .= " FROM ".MAIN_DB_PREFIX."commande as c";
	$sql .= ", ".MAIN_DB_PREFIX."societe as s";
	$sql .= ", ".MAIN_DB_PREFIX."categorie_societe as cs";
	$sql .= " WHERE c.fk_soc = s.rowid";
	$sql .= " GROUP BY s.nom";
	$sql .= " ORDER BY mtotal DESC";
	$sql .= " LIMIT ".$conf->global->NBR_CLIENTS;

	$resql = $db->query($sql);

	if ($resql)
	{
		$total = 0;
		$num = $db->num_rows($resql);

		print '<table class="noborder centpercent">';
		print '<tr class="liste_titre">';
		print '<th colspan="2">'.$langs->trans("PClients").($num?'<span class="badge marginleftonlyshort">'.$num.'</span>':'').'</th>';
		print '<th colspan="2" class="right">'.$langs->trans("Montant").'</span>'.'</th></tr>';

		$var = true;
		if ($num > 0)
		{
			$i = 0;
			while ($i < $num)
			{
				$obj = $db->fetch_object($resql);
				print '<tr class="oddeven">';

				$lessocietes->nom=$obj->nom;
				$lessocietes->rowid=$obj->rowid;
				$lescommandes->mtotal=$obj->mtotal;
				$lessocietes->fk_categorie=$obj->fk_categorie;
				$lessocietes->csoc=$obj->csoc;
	

				print '</td>';

				print '<td  colspan="2" class="nowrap"><a class="right" class="nowrap" href="'.DOL_URL_ROOT.'/societe/card.php?socid='.((int) $obj->rowid).'">'.'<span class="fas fa-building paddingrightonly"></span>'.$obj->nom.'</a></td> <td  colspan="2" class="right" class="nowrap">'.price($obj->mtotal).'</td> </tr>';
			
				$i++;
			}
		}
		else
		{

			print '<tr class="oddeven"><td colspan="3" class="opacitymedium">'.$langs->trans("NoOrder").'</td></tr>';
		}
		print "</table><br>";

		$db->free($resql);
	}
	else
	{
		dol_print_error($db);
	}
}



print '</div><div class="fichetwothirdright">';


$NBMAX = $conf->global->MAIN_SIZE_SHORTLIST_LIMIT;
$max = $conf->global->MAIN_SIZE_SHORTLIST_LIMIT;




// meilleurs produits


if (! empty($conf->clients->enabled) && $user->rights->clients->client->read)
{
	$langs->load("orders");

// ajout du premier produit dans la categorie
	$sql = "SELECT cd.rowid, cd.fk_product, SUM(cd.qty) as tqty, p.rowid as rowid, p.ref, p.description, cp.fk_product, cp.fk_categorie";
	$sql.= " FROM ".MAIN_DB_PREFIX."commandedet as cd";
	$sql .= ", ".MAIN_DB_PREFIX."product as p";
	$sql .= ", ".MAIN_DB_PREFIX."categorie_product as cp";
	$sql.= " WHERE cd.fk_product = p.rowid";
	$sql .= " GROUP BY cd.fk_product";
	$sql .= " ORDER BY tqty DESC";
	$sql .= " LIMIT 1";

	$resql = $db->query($sql);

	if ($resql)
	{
		$obj = $db->fetch_object($resql);

		$sql = "REPLACE INTO ".MAIN_DB_PREFIX."categorie_product (fk_categorie, fk_product)";
		$sql .= " VALUES (";
		$sql .= "208,";
		$sql .= "'".$obj->fk_product."'"; 
		$sql .= ")";
		if (!$db->query($sql)) {
			dol_print_error($db);
			$error++;
		}

	}

	// affichage des informations
	$sql = "SELECT cd.rowid, cd.fk_product, SUM(cd.qty) as tqty, p.rowid as rowid, p.ref, p.description,  cs.fk_soc as csoc, cp.fk_product, cp.fk_categorie";
	$sql.= " FROM ".MAIN_DB_PREFIX."commandedet as cd";
	$sql .= ", ".MAIN_DB_PREFIX."product as p";
	$sql .= ", ".MAIN_DB_PREFIX."categorie_product as cp";
	$sql .= ", ".MAIN_DB_PREFIX."categorie_societe as cs";
	$sql.= " WHERE cd.fk_product = p.rowid";
	$sql .= " GROUP BY cd.fk_product";
	$sql .= " ORDER BY tqty DESC";
	$sql .= " LIMIT ".$conf->global->NBR_PRODUITS;

	$resql = $db->query($sql);

	if ($resql)
	{
		$total = 0;
		$num = $db->num_rows($resql);

		print '<table class="noborder centpercent">';
		print '<tr class="liste_titre">';
		print '<th colspan="2">'.$langs->trans("PProducts").($num?'<span class="badge marginleftonlyshort">'.$num.'</span>':'').'</th>';
		print '<th colspan="2" class="right">'.$langs->trans("Nombre").'</span>'.'</th> ';
		print '<th colspan="2" class="right">'.$langs->trans("Description").'</span>'.'</th>  </tr>';

		$var = true;

		if ($num > 0)

		{
			$i = 0;
			while ($i < $num)
			{

				$obj = $db->fetch_object($resql);
				print '<tr class="oddeven">';

				$lescommandes->fk_product=$obj->fk_product;
				$lescommandes->tqty=$obj->tqty;
				$lesproduits->description=$obj->description;
				$lesproduits->ref=$obj->ref;
				$lescategories->fk_product=$obj->fk_product;
				$lescategories->fk_categorie=$obj->fk_categorie;
				
				print '</td>';

				print '<td colspan="2" class="nowrap">'.'<a class="nowrap" href="'.DOL_URL_ROOT.'/product/card.php?id='.((int) $obj->rowid).'">'.'<span class="fa fa-cube paddingrightonly"></span>'.$obj->ref.'</a>'.'</td> <td colspan="2" class="right" class="nowrap">'.$obj->tqty.'</td> <td colspan="2" class="right" class="nowrap">'.$obj->description.'</td> </tr>';
				$i++;
			}


				// ajout du resultat dans la bdd
				$sql = "INSERT INTO ".MAIN_DB_PREFIX."clients_client (produit, client)";
				$sql .= " VALUES (".$obj->fk_product.", ".$obj->csoc.")";
				if (!$db->query($sql)) {
					dol_print_error($db);
					$error++;
				}

		}
		else
		{
			print '<tr class="oddeven"><td colspan="3" class="opacitymedium">'.$langs->trans("NoOrder").'</td></tr>';
		}
		print "</table><br>";
 
		$db->free($resql);
	}
	else
	{
		dol_print_error($db);
	}
} 


print '</div><div class="fichetwothirdright">';


$NBMAX = $conf->global->MAIN_SIZE_SHORTLIST_LIMIT;
$max = $conf->global->MAIN_SIZE_SHORTLIST_LIMIT;

print '</div></div>';

// End of page
llxFooter();
$db->close();
