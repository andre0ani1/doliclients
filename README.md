# Module DoliClients pour [DOLIBARR ERP CRM](https://www.dolibarr.org)

## Fonctionnalités

Ce module permet d'afficher les clients ayant fait le plus gros montant de commandes ainsi que les produits qui rapportent le plus.



## License

GPLv3


### Documentation

La documentation est disponible avec le code sur le [gitlab du projet](https://gitlab.com/andre0ani1/doliclients)


### Todo
- Afficher sous forme de graphique ?
- Avoir un suivi par mois ?
- Configuration pour choisir combien de clients/produits on affiche ?
- Liens clients/produits clicable ?
